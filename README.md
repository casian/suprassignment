PREREQUISITES: Erlang Virtual Machine (EVM) available from "https://www.erlang.org/downloads".
COMPILATION INSTRUCTIONS:
1. Open the Erlang shell (erl or werl).
2. Navigate to the source directory by writing the following.
    cd("C:\\example_dest_path\\src").
Please change "example_dest_path" accordingly and be sure to end the instruction with a fullstop.
3. Compile the files using
    make:all().
4. Done

EXECUTION INSTRUCTIONS:
1. Open the Erlang shell (erl or werl).
2. Navigate back to the source directory by writing the following.
    cd("C:\\example_dest_path\\src").
3. Execute one of the following:
    a) To input the triangle tree directly using the console write:
            suprnation:read_console().
    b) To input the tree from a text file write:
            suprnation:read_textfile("C:\\path_to_file").
    c) To run a predefined test directly from one of the test files use either one of the following:
            test:test_file().
            test:test_file_2().
    d) To run (hardcoded) tests that execute the algorithm directly use
            test:test_tree().
