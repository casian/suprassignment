%%%-------------------------------------------------------------------
%%% @author Ian Cassar
%%% Created : 20. Oct 2019 12:15
%%%-------------------------------------------------------------------
-module(test).
-author("Ian Cassar").

%% API
-compile(export_all).

test_tree()->
  triangle_tree:find_minimal_path(
    [
      [11,2,10,9],
      [3,8,5], %% Starting Point
      [6,3],
      [7]
    ]).

test_file()->
  suprnation:read_textfile("test_file.txt").

test_file_2()->
  suprnation:read_textfile("test_file_2.txt").