%%%-------------------------------------------------------------------
%%% @author Ian Cassar
%%% Created : 20. Oct 2019
%%%-------------------------------------------------------------------
-module(suprnation).
-author("Ian Cassar").

%% API
-export([read_console/0,read_textfile/1]).

%% -------------------------------------------------------------------
%% @doc
%% Function: read_console/0.
%% Status: Public
%% Input: N/A
%% Method: Allows the user to input a triangle tree via the standard input
%% console for which the minimal path is computed if it is provided in the
%% correct format.
%% -------------------------------------------------------------------
read_console() ->
  io:format("Please input a top down triangle tree.~n",[]),
  read_file(standard_io).

%% -------------------------------------------------------------------
%% @doc
%% Function: read_textfile/1.
%% Status: Public
%% Input: Path to Text File
%% Method: Allows the user to input the path to a text file defining
%% a triangle tree. The minimal path is then computed if the tree provided
%% abides by the correct format.
%% -------------------------------------------------------------------
read_textfile(FilePath) ->
  File = file:open(FilePath, [read]),
  case File of
    {ok,Device} ->
      read_file(Device);
    {error,Error} ->
      io:format("An error has occured while opening this file: ~p",[Error])
  end.

%% -------------------------------------------------------------------

%% -------------------------------------------------------------------
%% @doc
%% Function: read_file/1.
%% Status: Private
%% Input: An IOdevice (stdin or file)
%% Method: This method opens the file (or stdin) and parses it using
%% parse_file/1. It then forwards the parsed tree to triangle_tree:find_minimal_path/1
%% to compute the minimal path. If successful the path is pretty printed.
%% -------------------------------------------------------------------
read_file(Device) ->
    ParsedTree = parse_file(Device),
    Result = triangle_tree:find_minimal_path(ParsedTree),
    case Result of
      {ok,MinPath} ->
        print_result(MinPath);
      {error,_Error} ->
        io:format("The tree provided is not a valid triangle tree.",[])
    end.

%% -------------------------------------------------------------------
%% @doc
%% Function: parse_file/1.
%% Status: Private
%% Input: An IOdevice (stdin or file)
%% Method: This method parses the file (or stdin) line by line.
%% Each line is validated and converted into a triangle tree format
%% that is then used to the minimal path.
%% -------------------------------------------------------------------
parse_file(Device) ->
  case io:get_line(Device, "") of
    eof  -> []; %% Required when reading from a file unless the user specifically writes EOF.
    Line ->
      CleanedLine= string:trim(Line), %% Cleaning trailing characters.
      %% 1) Is the current line valid?
      case is_line_valid(Line) of
        true ->  %% 1) Yes
          SplitLine=re:split(CleanedLine,"\\s"), %% Splitting the line for each whitespace.
          NumberList = lists:map(fun(Num) -> binary_to_integer(Num) end,SplitLine), %% Converting every value to an integer
          io:format("NumberList: ~p~n",[NumberList]),
          parse_file(Device) ++ [NumberList]; %% Moving to the next line.
        false -> %% 1) No
          %% 2) Does this invalid line specify EOF?
          case is_line_eof(Line) of
            true -> []; %% Yes (then ok)
            false -> io:format("The provided input has an invalid format",[]) %% No (then error)
          end
      end
  end.

%% -------------------------------------------------------------------
%% @doc
%% Function: is_line_valid/1.
%% Status: Private
%% Input: A Line
%% Method: Uses a regular expression to check whether the line defines a
%% character that is neither a number nor a space.
%% -------------------------------------------------------------------
is_line_valid(Line) ->
  case re:run(Line,"[^(0-9|\\s|\\-)]") of
    nomatch -> true;
    _ -> false
  end.

%% -------------------------------------------------------------------
%% @doc
%% Function: is_line_eof/1.
%% Status: Private
%% Input: A Line
%% Method: Uses a regular expression to check whether the line specifies
%% the text EOF.
%% -------------------------------------------------------------------
is_line_eof(Line) ->
  case re:run(Line,"EOF|eof") of
    nomatch -> false;
    _ -> true
  end.

%% -------------------------------------------------------------------
%% @doc
%% Function: print_result/1.
%% Status: Private
%% Input: A minimal path result
%% Method: Prints onscreen the minimal path.
%% -------------------------------------------------------------------
print_result({Val,[HPath|TPath]}) ->
  io:format("Minimal Path is: ~p ",[HPath]),
  lists:foreach(fun(Node) -> io:format("+ ~p ", [Node]) end,TPath),
  io:format("= ~p ",[Val]).


