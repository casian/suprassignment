%%%-------------------------------------------------------------------
%%% @author Ian Cassar
%%% Created : 20. Oct 2019 12:08
%%%-------------------------------------------------------------------
-module(triangle_tree).
-author("Ian Cassar").

%% API
-export([find_minimal_path/1]).

%% -------------------------------------------------------------------
%% @doc
%% Function: find_minimal_path/1.
%% Status: Public
%% Input: A Tree
%% Produces: The Minimal Path
%% Method: Validates the tree via the function is_tree_valid/1 and if valid
%% it calls the minimal_path_rec\3 to perform a bottom up traversal starting
%% from the leaves towards the root.
%% -------------------------------------------------------------------
find_minimal_path(Tree) ->
  case is_tree_valid(Tree) of
    true ->
      MinPath = minimal_path_rec(Tree,[], length(Tree)),
      {ok,MinPath};
    false -> {error,invalid_tree}
  end.

%% -------------------------------------------------------------------
%% @doc
%% Function: is_tree_valid/1.
%% Status: Private
%% Input: A Tree
%% Method: Checks that the given tree has the correct structure, that is
%% the tree is a list of levels. Each level must be a list that contains
%% the right amount of nodes. Also checks that each node defines a number.
%% -------------------------------------------------------------------

is_tree_valid([]) -> true;
is_tree_valid(Tree) ->
  %% 1) Is the tree actually a list?
  if is_list(Tree) ->   %% 1) yes
    TreeDepth = length(Tree),
    [LowestLevel | RestOfTree] = Tree,
    %% 2) Is the triangle reversed? And so, does the current (topmost) level
    %% define the same number of elements as the tree's depth?
    if TreeDepth == length(LowestLevel) ->
      %2) Yes
      %% 3) Do all nodes in the current level define numbers only?
      case lists:all(fun(Node) -> is_number(Node) end, LowestLevel) of
        true -> is_tree_valid(RestOfTree); %% 3) Yes
        false -> false %% 3) No
      end;
      true ->
        %2) no
        false
    end;
      true -> false %% 1) no
  end.


%% -------------------------------------------------------------------
%% @doc
%% Function: minimal_path_rec/3.
%% Status: Private
%% Input: Root of Triangle Tree, Tree of Child Weights (initially empty)
%% and the Tree Depth.
%% Method: Consists in two parts, the BASE CASE and the INDUCTIVE CASE.
%% The inductive case starts from the largest (bottommost) level in the
%% tree and computes the least expensive path which we refer to as the
%% "weights". These weights are stored in the WeightTree which is also
%% a triangle tree but each of its nodes stores a weight in the following
%% format: {Weight,Path} where the Weight is the calculated minimal sum
%% from the leaves to the current node, while Path stores the current
%% minimal path from the leaf to the current node.
%% -------------------------------------------------------------------

minimal_path_rec([RootLevel], WeightTree ,_TreeDepth) -> %%BASE CASE
  [ChildLevelWeights|_] = WeightTree,
  [FinalRes] = compute_level_weights(RootLevel,ChildLevelWeights,[]),
  FinalRes;
minimal_path_rec(Tree, WeightTree, TreeDepth) -> %%INDUCTIVE CASE
  [CurrentLevel| OtherLevels] = Tree,
  %% Is this the largest (bottommost) level?
  if length(Tree) == TreeDepth ->
    %% Yes: Create the first level in the weights tree.
    BottomMostLevelWeights = lists:map(fun(Node) -> {Node,[Node]} end, CurrentLevel), %% Wrapping
    minimal_path_rec(OtherLevels, [BottomMostLevelWeights] , TreeDepth);
  true ->
    %% No: Extract the weights of the child level.
    [ChildLevelWeights|_] = WeightTree,
    %% Compute the weights of the current level.
    CurrentLevelWeights = compute_level_weights(CurrentLevel,ChildLevelWeights,[]),
    %% Update the WeightTree with the new weights.
    NewWeightTree = [CurrentLevelWeights| WeightTree],
    %% Recurse to check the smaller (upper) levels.
    minimal_path_rec(OtherLevels, NewWeightTree, TreeDepth)
  end.



%% -------------------------------------------------------------------
%% @doc
%% Function: inner_loop/3.
%% Status: Private
%% Input: The current tree level, weight list of the child levels and
%% the partially computed weight list (required for tail recursion).
%% Method: Each node in the current level is inspected, vis-a-vis the
%% weights of its two children. As a result we populate a new level of
%% weights. Each new weight defines the minimal path (and its sum) it
%% takes to be reached from one of its descendant leaves.
%% -------------------------------------------------------------------

compute_level_weights([],_,CurWeightList) -> % Base Case
  % The iteration ends when we processed all nodes in the level.
  CurWeightList;
compute_level_weights([LevelHead|LevelTail],ChildWeightList,CurWeightList) ->  % Inductive Case
  [{NL,PathL}|FirstChildWeightTail] = ChildWeightList, % Gets the weight of the LEFT Child
  [{NR,PathR}|_] =FirstChildWeightTail,  % Gets the weight of the RIGHT Child
  % Which child leads to the least expensive path so far?
  {ChosenNode,ChosenPath} = if NL < NR -> {NL,PathL}; true -> {NR,PathR} end,
  % Compute the new weight and path.
  NewWeight = ChosenNode+LevelHead,
  NewPath = [LevelHead|ChosenPath],
  % Update the weight list for the current level.
  NewCurWeightList = CurWeightList ++ [{NewWeight,NewPath}],
  % To keep on inspecting the next node in the current level, we remove the leftmost child weight from the ChildWeightList
  % since it is not the child of the next node in the current level. We therefore recurse using the tail of the current level "LevelTail",
  % the newly computed weight list for the current level "NewCurWeightList" and the tail of the ChildWeightList that removes the
  % leftmost child, ie FirstChildWeightTail.
  compute_level_weights(LevelTail,FirstChildWeightTail,NewCurWeightList).





